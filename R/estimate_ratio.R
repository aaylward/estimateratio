#' @title Estimate the ratio of two random variables from a sample
#'
#' @description Estimate the ratio of two random variables from a sample
#' 
#' @param x vector of observations for denomenator
#' @param y vector of observations for numerator
#' @param conf confidence level for confidence intervals
#' @return \describe{
#'   \item{r}{the estimate of the ratio}
#'   \item{variance}{estimated variance of r}
#'   \item{sd}{estimated standard deviation of r}
#'   \item{ci}{
#'     confidence interval estimated using the Vysochanskij–Petunin inequality
#'   }
#' }
#' @export
estimate_ratio <- function(x, y, conf=0.95) {
  m_x <- mean(x)
  m_y <- mean(y)
  r <- m_y / m_x
  s_r_2 <- 1 / (length(x) - 1) * sum((y - r * x) * (y - r * x))
  var_r <- 1 / m_x^2 * s_r_2 / length(x)
  sd_r <- sqrt(var_r)
  l = 2 / (3 * sqrt(1 - conf))
  list(
    r = r,
    variance = var_r,
    sd = sd_r,
    ci = c(r - sd_r * l, r + sd_r * l)
  )
}